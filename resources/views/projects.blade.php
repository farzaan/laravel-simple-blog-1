@extends('layout')

@section('header-title')
    <section id="home" class="parallax-section">
        <div class="overlay"></div>
    <!-- Video -->
    <video controls autoplay loop muted>
        <source src="/videos/video.mp4" type="video/mp4">
        Your browser does not support the video tag.
    </video>
    </section>
@endsection

@section('content')
    <section id="about" class="parallax-section">

    <!-- PROJECT -->

        <h1 style="text-align: center; padding: 20px;">Our Projects</h1>

        <div class="container">
            <div class="row">


                    @foreach($articles as $article)
                    <div class="col-md-6 col-sm-6">
                    <!-- PROJECT ITEM -->
                    <div class="project-item">
                        <a href="images/project-image{{$article->id <=6 ? $article->id : 1}}.jpg" class="image-popup">
                            <img src="images/project-image{{$article->id <=6 ? $article->id : 1}}.jpg" class="img-responsive" alt="">

                            <div class="project-overlay">
                                <div class="project-info">
                                    <h1>{{$article->title}}</h1>
                                    <h3>{{$article->excerpt}}</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                @endforeach


            </div>
        </div>
    </section>

@endsection
