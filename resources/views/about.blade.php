@extends('layout')

@section('header-title')
    <section id="home" class="parallax-section">
        <div class="overlay"></div>
    <!-- Video -->
    <video controls autoplay loop muted>
        <source src="/videos/video.mp4" type="video/mp4">
        Your browser does not support the video tag.
    </video>

    </section>
@endsection

@section('content')
    <!-- ABOUT -->
    <section id="about" class="parallax-section" style="padding-top: 50px">
        <div class="container">
            <div class="row">

                <div class="col-md-offset-1 col-md-10 col-sm-12">
                    <div class="about-info">
                        <h3>Introducing Scenic</h3>
                        <h1>This template is designed for you. Sed ornare, tortor nec placerat lacinia, leo quam rutrum leo, eget posuere ipsum sem eu justo. Integer nunc libero.</h1>
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection
