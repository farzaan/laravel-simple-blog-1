@extends('layout')

@section('header-title')

    <section id="home" class="parallax-section">
        <div class="overlay"></div>
    <div class="container">
        <div class="row">

            <div class="col-md-8 col-sm-12">
                <div class="home-text">
                    <h1>{{$article->title}}</h1>
                    <p>{{$article->excerpt}}</p>
                    <small style="color: whitesmoke">{{$article->created_at}}</small>
                </div>
            </div>

        </div>
    </div>

    <!-- Video -->
    <img src="/images/project-image{{$article->id ??1}}.jpg" class="img-responsive" alt="" style="position: absolute; top: 0; right: 0; bottom: 0; left: 0; width: 100%; height: 100%; z-index: -10;">
    </section>

@endsection


@section('content')
    <section id="articles" class="parallax-section">
<div class="container" style="text-align: center; height: 200px;">
    <div class="row">
        <div class="col-md-8 col-sm-12">
    <p style="margin-top: 50px">{{$article->body}}</p>
    <!--<img src="/images/project-image{{--$article->id--}}.jpg" class="img-responsive" alt="" style="width: 50vh">-->

    @foreach($article->tag as $tag)
        <!--<a href="tags/{{--$tag->name--}}">{{--$tag->name--}}</a><br>-->
        <!--<a href="/articles?tag={{--$tag->name--}}">{{--$tag->name--}}</a><br>-->
        <a href="{{route('article.index', ['tag'=>$tag->name])}}">{{$tag->name}}</a><br>
        @endforeach
    </div>
    </div>
</div>
    </section>

@endsection
