@extends('layout')

@section('header-title')
    <section id="home" class="parallax-section smallHeader">
        <div class="overlay"></div>

    </section>
@endsection

@section('content')


    <div class="container" style="margin-top: 50px;">
        <h1>Edit Article</h1>

        <form method="POST" action="/articles/{{$article->id}}">

            @csrf
            @method('PUT') <!-- nowadays browsers just know GET and PUT methods. So we define PUT methode like this-->

            <div class="form-group">
                <label for="articleTitle">Article Title</label>
                <input type="text" class="form-control" id="articleTitle" name="title" value="{{$article->title}}">
            </div>

            <div class="form-group">
                <label for="articleExcerpt">Article Excerpt</label>
                <textarea class="form-control" id="articleExcerpt" name="excerpt" rows="2" placeholder="New Article Excerpt">{{$article->excerpt}}</textarea>
            </div>

            <div class="form-group">
                <label for="articleBody">Article Body</label>
                <textarea class="form-control" id="articleBody" name="body" rows="5" placeholder="New Article body">{{$article->body}}</textarea>
            </div>

            <button type="submit" id="addArticleSubmit" class="btn btn-primary">Submit</button>
        </form>
    </div>

@endsection
