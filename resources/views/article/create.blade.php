@extends('layout')

@section('header-title')
    <section id="home" class="parallax-section" style="height: 20%">
        <div class="overlay"></div>


    </section>
@endsection


@section('content')

    <div class="container" style="margin-top: 50px;">
        <h1>New Article</h1>

    <form method="POST" action="/articles">

        @csrf

        <div class="form-group">
            <label for="title">Article Title</label>
            <input id="title" type="text" name="title" class="form-control" style="@error('title') border: 1px solid #a94442 @enderror" placeholder="New Article title" value="{{old('title')}}">

            @error('title')
            <div class="invalid-feedback" style="color: #a94442">
                {{$errors->first('title')}}
            </div>
            @enderror

        </div>

        <div class="form-group">
            <label for="excerpt">Article Excerpt</label>
            <textarea class="form-control" id="excerpt" name="excerpt" rows="2" style="@error('excerpt') border: 1px solid #a94442 @enderror" placeholder="New Article Excerpt">{{old('excerpt')}}</textarea>

    {{--
   @foreach ($errors->all() as $error)
       <li>{{ $error }}</li>
   @endforeach
   --}}


   @error('excerpt')
            <div class="invalid-feedback" style="color: #a94442">
       {{$errors->first('excerpt')}}
   </div>
   @enderror

</div>

<div class="form-group">
   <label for="body">Article Body</label>
   <textarea class="form-control" id="body" name="body" style="@error('body') border: 1px solid #a94442; @enderror" rows="4" placeholder="New Article body">{{old('body')}}</textarea>


   @error('body')
    <div class="invalid-feedback" style="color: #a94442">
       {{$errors->first('body')}}
   </div>
   @enderror

</div>

        <div class="form-group">
            <label for="exampleFormControlSelect2">Select Article Tags</label>
            <select multiple name="tags[]" class="form-control" id="exampleFormControlSelect2">
                @foreach($tags as $tag)
                <option value="{{$tag->id}}">{{$tag->name}}</option>
                @endforeach
            </select>

            @error('tags')
            <div class="invalid-feedback" style="color: #a94442">{{$message}}</div>
            @enderror

        </div>

<button type="submit" id="addArticleSubmit" class="btn btn-primary">Submit</button>
</form>
</div>
@endsection
