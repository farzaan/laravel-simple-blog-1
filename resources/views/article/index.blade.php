@extends('layout')

@section('header-title')
    <section id="home" class="parallax-section">
        <div class="overlay"></div>
    <!-- Video -->
    <video controls autoplay loop muted>
        <source src="/videos/video.mp4" type="video/mp4">
        Your browser does not support the video tag.
    </video>
    </section>
@endsection

@section('content')
    <section id="about" class="parallax-section">
    @forelse($articles as $article)
        <div style="padding:0 50px;text-align: center;">
        <a href="{{route('article.show',$article)}}"><h1>{{$article->title}}</h1></a>
    <p>{{$article->excerpt}}</p>
        </div>
        <hr>
        @empty
                <p>sorry! no posts for this tag yet...</p>
    @endforelse
    </section>
@endsection
