<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Article;
use App\User;
use Faker\Generator as Faker;

$factory->define(Article::class, function (Faker $faker) {
    return [
        'user_id' => factory(User::class),//return new user for each article...
        'title' => $faker->sentence,
        'excerpt' => $faker->sentence,
        //'body' => $faker->paragraphs(1)
        'body' => $faker->paragraph
    ];
});
