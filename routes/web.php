<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/about', function () {
    return view('about');
});

Route::get('/projects', function () {

    //$articles = App\Article::all();
    //$articles = App\Article::take(2)->get();
    //$articles = App\Article::paginate(2);
    //$articles = App\Article::latest()->get(); //order by created_at DESC
    //$articles = App\Article::latest('updated_at')->get(); //order by updated_at DESC
    $articles = App\Article::take(6)->latest()->get();


    //dd($articles);

    return view('projects',['articles'=>$articles]);
});



//Order Matters
Route::get('/articles', 'ArticleController@index')->name('article.index');
Route::get('/articles/create', 'ArticleController@create');
Route::get('/articles/{article}/edit', 'ArticleController@edit'); //wildcards iare better to name as their Model class
Route::get('/articles/{article}', 'ArticleController@show')->name('article.show');
Route::post('/articles', 'ArticleController@store');
Route::put('/articles/{article}', 'ArticleController@update');






//REST routing------------------------------------------------------

// GET, POST, PUT, DELETE

// GET /articles                ->list of resources
// GET /articles/:id            ->single rosource
// GET /articles/create         ->show a form to add resource
// POST /articles               ->send added resource to database [to create new resource]
// GET /articles/:id/edit       ->edit a rosource with given id
// PUT /articles/:id            ->update a resource with a given id
// DELETE /articles/:id         ->delete a resource with a given id
//--------------------------------------------------------------------







//--------------------------------------------------------------------

Route::get('/test', function () {//get variables through URL (http://127.0.0.1:1111/test/?name=samaneh)

    $var = request("name");
    return view('test',["name"=>$var]);
});


Route::get('/post/{post}', function ($post) {//get variables through wildcard from URL (http://127.0.0.1:1111/post/1)

    $posts =[//simulate a DB
        "1"=>["post 1 title","post 1 content"],
        "2"=>["post 2 title","post 2 content"]
    ];

    if(!array_key_exists($post,$posts)){//handle invalid post id
        abort(404,"Sorry! Nothing Found");
    }

    return view('post',["postTitle"=>$posts[$post][0] ?? "nothing Found", "postContent"=>$posts[$post][1] ?? ""]);
});


Route::get('posts/{postId}','PostsController@showById');//send requests to Controller


