<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    public function complete(){ // Business logic of completing an assignment
        $this->completed = true;
        $this->save();
    }
}
