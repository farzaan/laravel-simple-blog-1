<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    //




    //define relations between different entities of Project (here: user, article, project)

    public function user(){ //project1->user();
        $this->belongsTo(User::class); //produced sql query: SELECT * FROM user where project_id = 1;
    }
}
