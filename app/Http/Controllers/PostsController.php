<?php

namespace App\Http\Controllers;

use App\Posts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller
{
    public function showById($postId){

        /*$post = DB::table('posts')->where('id',$postId)->first();//queryBuilder api

        //dd($post);

        if(!$post){
            abort(404,"Sorry, Nothing Found!");
        }
        */

        $post = Posts::where('id',$postId)->firstOrFail();

        return view('post',["post"=>$post, "postTitle"=>$post->title ?? "nothing Found", "postContent"=>$post->content ?? ""]);

    }
}
