<?php

namespace App\Http\Controllers;

use App\Article;
use App\Tag;
use Illuminate\Http\Request;


//cmd command to create this Controller:
//php artisan make:controller ArticleController -r(=resource controller) -m(model) Article(model name)


class ArticleController extends Controller //7 Restful Controller Actions
{
    //Render a list of Resource
    public function index(){

        if(request('tag')){
            $articles = Tag::where('name',request('tag'))->firstOrFail()->articles->sortByDesc('created_at');;
        }else{
            $articles = Article::latest()->get();
        }
        return view('article.index',['articles'=>$articles]);
    }

    //Show a siingle Resource
    /*public function show($articleId){
        $article = Article::findOrFail($articleId);
        return view('article.show',['article'=>$article]);
    }*/

    //Show a siingle Resource
    public function show(Article $article){ //wildcard name is important for this type of arguments (if wildcard name is articleTitle, argument name should also be the same)
        return view('article.show',['article'=>$article]);
    }

    public function create(){
        //shows a view to create a new resource

        $tags = Tag::all();

        return view('article.create',['tags'=>$tags]);

    }

    public function store(){

        //dd(request()->all());

        //Persist the resource [created by form]
        //dump(request()->all());

        /*$validatedAttributes = request()->validate([
           'title' => 'required',
           'excerpt' => 'required',
           'body' => 'required'
        ]);*/ //instead-------------we define a validation method
            //$validatedAttributes=$this->getValidate();

            $this->getValidate();
            $article = new Article(request(['title','excerpt','body']));
            $article->user_id = 2;
            $article->save();

            $article->tag()->attach(request('tags'));


        /*
        $article = new Article();
        $article->title = request('title');
        $article->excerpt = request('excerpt');
        $article->body = request('body');
        $article->save();
        */ //instead------------------below-----------------

        /*Article::create([ //$fillable required in Model
            'title' => request('title'),
            'excerpt' => request('excerpt'),
            'body' => request('body')
        ]);*/ //instead---------------below----------------


        //Article::create($article);


        //return redirect('/articles');
        return redirect(route('article.index'));
    }

    public function edit(Article $article){
        //show a view to edit an existing Resource
        return view('article.edit',compact('article'));
    }

    public function update(Article $article){
        //persist the edited Resource

        $validatedAttributes = $this->getValidate();

        $article->update($validatedAttributes);

        //return redirect('/articles/'.$article->id);
        return redirect(route('article.show',$article)/*second parameter is wildcard defenition (can be $article->id)*/);
    }

    public function destroy(){
        //Delete the resource
    }


    public function getValidate(): array
    {
        return request()->validate([
            'title' => 'required',
            'excerpt' => 'required',
            'body' => ['required', 'min:20', 'max:2047'],
            'tags' => 'exists:tags,id'
        ]);
    }

}
